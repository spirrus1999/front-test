const path = require('path');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';
const min = isProd ? '.min' : '';

const cssLoaders = (value) => {
    const loaders = [
        MiniCssExtractPlugin.loader,
        'css-loader',
    ];

    if (value) {
        loaders.push(value);
    }

    return loaders;
}

const pluginLoaders = () => {
    const plugins = [
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: [
                '**/*.LICENSE.txt',
                '**/vendor.min.js.map'
            ],
            protectWebpackAssets: false
        }),
        new MiniCssExtractPlugin({
            filename: 'css/styles' + min + '.css'
        }),
        new webpack.ProvidePlugin({
            $:"jquery",
            jQuery:"jquery",
            "window.jQuery":"jquery"
        }),
        !isProd ?
            new HtmlWebpackPlugin({
                template: 'index.html'
            }) :
            new CopyPlugin({
                patterns: [
                    {
                        from: path.resolve(__dirname, 'src/index.html')
                    },
                    {
                        from: path.resolve(__dirname, 'src/assets/img.jpg'),
                        to: 'assets'
                    }
                ]
            })
    ];

    return plugins;
}

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: "none",
    entry: './js/tabs.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/scripts' + min + '.js'
    },
    devtool: 'source-map',
    plugins: pluginLoaders(),
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },
    optimization: {
        minimize: true,
        splitChunks: {
            filename: 'js/vendor' + min + '.js',
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/i,
                use: cssLoaders()
            },
            {
                test: /\.s[ac]ss$/i,
                use: cssLoaders('sass-loader')
            }
        ]
    },

    devServer: {
        port: '3000'
    }
};

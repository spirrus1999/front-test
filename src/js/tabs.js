import '@/scss/styles.scss';

require('jquery-ui-bundle');

(function ($) {

    $(document).on('scroll', function () {
        let svg = $('.header').find('svg')
        if (pageYOffset > 50) {
            svg.css('height', '50px');
        } else {
            svg.css('height', '');
        }
    })

    $('#draggable').draggable();

    $('#mouse').mousemove(function (e) {
        let position = $('#mouse').position();
        let width = this.clientWidth;
        let height = this.clientHeight;
        let x = (e.clientX - position.left) / width;
        let y = (e.clientY - position.top) / height;
        let xx = (-1920 + width) * x;
        let yy = (-1080 + height) * y;
        $('.img').css({
            'top': yy + 'px',
            'left': xx + 'px'
        });
    });

    $(".tab-header-btn").click(function (e) {
        $(e.target).next('ul').slideToggle('fast');
        $('.tab-header-btn').text($('.tab-header-wrap').find('li.active').text());
    });

    'use strict';

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self.find('.js-tab-header').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 1;
        });
        var $tabContent = $self.find('.js-tab-content').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 1;
        });

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass('active').eq(index).addClass('active');
            $tabContent.removeClass('active').eq(index).addClass('active');
        };

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on('click', function () {
                selectTab($(this).index());
            });
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $('.js-tabs').each(function () {
        $(this).data('tabs', $(this).tabs());
    });
})(jQuery);
